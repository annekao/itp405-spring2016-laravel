<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DVDSearchResultsTest extends TestCase
{
    public function testRedirectedBackToSearch()
    {
      $this
        ->visit('dvds/search')
        ->type('', 'dvdTitle')
        ->press('Search')
        ->seePageIs('/dvds?dvdTitle=&genre=Action&rating=G');
    }
}
