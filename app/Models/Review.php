<?php

namespace App\Models;

use DB;

class Review
{
  public function __construct($data)
  {
    $this->title = $data['title'];
    $this->description = $data['description'];
    $this->dvd_id = $data['dvd_id'];
    $this->rating = $data['rating'];
  }

  public function save()
  {
    DB::table('reviews')->insert([
      'title' => $this->title,
      'description' => $this->description,
      'dvd_id' => $this->dvd_id,
      'rating' => $this->rating
    ]);
  }

  public static function reviewsById($id) {
    return DB::table('reviews')
      ->select('dvd_id', 'title', 'rating', 'description')
      ->where('dvd_id', '=', $id)
      ->get();
  }
}
