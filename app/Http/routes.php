<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Services\API\NYTimes;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/nytimes/{date}', function($date) {
  $nytimes = new NYTimes([
    'apiKey' => 'f7f4325acd7108cf3f7b99853a406893:19:74630827'
  ]);

  $books = $nytimes->books("http://www.nytimes.com/best-sellers-books/$date/overview.html", $date);
  return view('nytimes', [
    'books' => $books,
    'date' => $date
  ]);
});

Route::group(['prefix' => 'api/v1', 'namespace' => 'API'], function() {
  // GET api/v1/genres
  Route::get('genres', 'GenreController@index');
  // GET api/v1/genres/{id}
  Route::get('genres/{id}', 'GenreController@show');
  
  // POST api/v1/dvds
  Route::get('dvds', 'DVDController@index');
  // GET api/v1/dvds/{id}
  Route::get('dvds/{id}', 'DVDController@show');

  Route::post('dvds', 'DVDController@store');

});
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
  Route::get('/dvds/create', 'DVDController@create');
  Route::get('/dvds/search', 'DVDController@search');
  Route::get('/dvds', 'DVDController@results');
  Route::get('/dvds/{id}', 'DVDController@review');
  Route::post('/dvds/create', 'DVDController@store');

  Route::get('/genres/{id}/dvds', 'GenreController@results');

  Route::post('/reviews/{id}', 'ReviewController@store');
});
