@extends('layout')

@section('content')
@foreach(array_chunk($books, 2) as $books)
<div class = "row">
  @foreach($books as $book)
    <div class="book col-md-6">
      <h1 class="rank">{{$book->rank}}</h1>
      <h4>{{$book->title}}<br>by {{$book->author}}</h4>
      <p>{{$book->description}}</p>
      <a href={{$book->amazon_product_url}}>Purchase</a>
    </div>
  @endforeach
</div>
@endforeach
@endsection

@section('title')
    New York Times Best Sellers<br>
    Combined Print & E-book Fiction<br>
    {{$date}}
@endsection
