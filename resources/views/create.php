<html>
<head>
  <title>DVD Create</title>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
  <link rel='stylesheet' type='text/css' href='/css/style.css'>
</head>
<body>


    <center>
  <?php if (session('success')) : ?>
  <div class="success">
    <h3>SUCCESS!</h3>
  </div>
  <?php endif ?>
  <?php if (count($errors) > 0) : ?>
  <ul>
    <li><h3>ERROR!</h3></li>
    <?php foreach ($errors->all() as $error) : ?>
      <li>
        <?php echo $error ?>
      </li>
    <?php endforeach ?>
  </ul>
  <?php endif ?>
    </center>

  <div id="create-outer-container">
  <h1><center>CREATE NEW DVD</center></h1>
  <form class="navbar-form" action="/dvds/create" method ="post">
    <?php echo csrf_field() ?>
    <div class="box-label">
      DVD Title<br>
    </div>
    <input type="text" class="form-control textbox" 
      placeholder="..." name="dvdTitle">

    <div class="box-label">
      Label<br>
    </div>
    <select class="form-control select" name="label">
      <?php foreach ($labels as $label) : ?> 
        <option value="<?php echo $label->id ?>"><?php echo $label->label_name ?></option>
      <?php endforeach; ?>
    </select>

    <div class="box-label">
      Sound<br>
    </div>
    <select class="form-control select" name="sound">
      <?php foreach ($sounds as $sound) : ?> 
        <option value="<?php echo $sound->id ?>"><?php echo $sound->sound_name ?></option>
      <?php endforeach; ?>
    </select>

    <div class="box-label">
      Genre<br>
    </div>
    <select class="form-control select" name="genre">
      <?php foreach ($genres as $genre) : ?> 
        <option value="<?php echo $genre->id ?>"><?php echo $genre->genre_name ?></option>
      <?php endforeach; ?>
    </select>

    <div class="box-label">
      Rating<br>
    </div>
    <select class="form-control select" name="rating">
      <?php foreach ($ratings as $rating) : ?> 
        <option value="<?php echo $rating->id ?>"><?php echo $rating->rating_name ?></option>
      <?php endforeach; ?>
    </select>

    <div class="box-label">
      Format<br>
    </div>
    <select class="form-control select" name="format">
      <?php foreach ($formats as $format) : ?> 
        <option value="<?php echo $format->id ?>"><?php echo $format->format_name ?></option>
      <?php endforeach; ?>
    </select>

    <input type="submit" class="btn btn-default" value="Submit">
  </form>
  </div>
</body>
</html>
