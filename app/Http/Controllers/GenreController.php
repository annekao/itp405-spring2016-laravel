<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Dvd;

class GenreController extends Controller
{
    public function results($id)
    {
      $dvds = Dvd::with('rating', 'label', 'genre')->where('genre_id', $id)->get();

      return view('genre/results', [
        'genre_name' => $dvds[0]->genre->genre_name,
        'dvds' => $dvds
      ]);
    }
}
