<html>
<head>
  <title>DVD Review</title>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
  <link rel='stylesheet' type='text/css' href='/css/style.css'>
</head>
<body>
  <meta charset="UTF-8">
  <title>DVD Review</title>
</head>
<body>
  
  <div id="outer-container2">

  <?php if (session('success')) : ?>
  <div class="success">
    <h3>SUCCESS!</h3>
  </div>
  <?php endif ?>

  <?php if (count($errors) > 0) : ?>
  <ul>
    <li><h3>ERROR!</h3></li>
    <?php foreach ($errors->all() as $error) : ?>
      <li>
        <?php echo $error ?>
      </li>
    <?php endforeach ?>
  </ul>
  <?php endif ?>

  <h2>
    '<?php echo $dvd->title ?>'
  </h2>
  <h4>
    <b>Release Date: </b>
    <?php echo date("F jS, Y", strtotime($dvd->release_date)) ?>
    <br>
    <b>Award:</b>
    <?php echo $dvd->award ?>
    <br>
    <b>Label:</b>
    <?php echo $dvd->label_name ?>
    <br>
    <b>Sound:</b>
    <?php echo $dvd->sound_name ?>
    <br>
    <b>Rating:</b>
    <?php echo $dvd->rating_name ?>
    <br>
    <b>Format:</b>
    <?php echo $dvd->format_name ?>
  </h4>
<hr>
  <div id="outer-container">
    <h2>Add a Review</h2>
  <form class="navbar-form" action="/reviews/<?php echo $dvd->id ?>" method ="post">
    <?php echo csrf_field() ?>
    <div class="box-label">
      Rating<br>
    </div>
    <select class="form-control select" 
    name="rating" value="<?php echo old('rating') ?>">
      <?php for ($i = 1; $i <= 10; $i++) : ?> 
        <option 
          value="<?php echo $i ?>"
          <?php if (old('rating') == $i) echo "selected" ?>>
          <?php echo $i ?></option>
      <?php endfor; ?>
    </select>

    <div class="box-label">
      Title<br>
    </div>
    <input type="text" class="form-control textbox" 
      name="title" 
      value="<?php echo old('title') ?>">

    <div class="box-label">
      Description<br>
    </div>
    <textarea class="form-control" rows="10" id="comment" name="description"><?php echo old('description') ?></textarea>
  </br><br>

    <input type="submit" class="btn btn-default" value="Submit">
  </form>

</div>
<br><br><br>
  <hr>
    <h2>Reviews</h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Title</th>
        <th>Rating</th>
        <th>Description</th>
      <tr>
    </thead>
    <tbody>
      <?php foreach ($reviews as $review) : ?>
        <tr>
          <th><?php echo $review->title; ?></th>
          <th><?php echo $review->rating; ?></th>
          <th><?php echo $review->description; ?></th>
        <tr>
    <?php endforeach; ?>
    </tbody>
  </table>

</div>
</body>
</html>
