<html>
<head>
  <title>DVD Search</title>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
  <link rel='stylesheet' type='text/css' href='/css/style.css'>
</head>
<body>
  <div id="side-bar">
    <h3><center><b>GENRES</b></center></h3>
      <?php foreach ($genres as $genre) : ?> 
        <a href=<?php echo '/genres/' . $genre->id . '/dvds'?> >
          <?php echo $genre->genre_name ?>
        </a><br>
      <?php endforeach; ?>

  </div>
  <div id="outer-container">
  <form class="navbar-form" action="/dvds" method ="get">
    <div class="box-label">
      DVD Title<br>
    </div>
    <input type="text" class="form-control textbox" 
      placeholder="..." name="dvdTitle">

    <div class="box-label">
      Genre<br>
    </div>

    <select class="form-control select" name="genre">
      <?php foreach ($genres as $genre) : ?> 
        <option value="<?php echo $genre->genre_name ?>"><?php echo $genre->genre_name ?></option>
      <?php endforeach; ?>
      <option value="All">All</option>
    </select>

    <div class="box-label">
      Rating<br>
    </div>
    <select class="form-control select" name="rating">
      <?php foreach ($ratings as $rating) : ?> 
        <option value="<?php echo $rating->rating_name ?>"><?php echo $rating->rating_name ?></option>
      <?php endforeach; ?>
      <option value="All">All</option>
    </select>

    <input type="submit" class="btn btn-default" value="Search">
  </form>
  </div>
</body>
</html>
