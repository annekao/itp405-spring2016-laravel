<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Models\Dvd;
use App\Models\Review;
use App\Models\Label;
use App\Models\Sound;
use App\Models\Genre;
use App\Models\Rating;
use App\Models\Format;

use Validator;
use DB;

class DVDController extends Controller
{
  public function store(Request $request)
  {
   $validation = Validator::make($request->all(), [
      'dvdTitle' => 'required',
      'label' => 'required',
      'sound' => 'required',
      'genre' => 'required',
      'rating' => 'required',
      'format' => 'required'
    ]);

    if ($validation->fails()) {
      return redirect('dvds/create')
        ->withInput()
        ->withErrors($validation);
    }

    $dvd = new Dvd();

    $dvd->title = $request->input('dvdTitle');
    $dvd->label_id = $request->input('label');
    $dvd->sound_id = $request->input('sound');
    $dvd->genre_id = $request->input('genre');
    $dvd->rating_id = $request->input('rating');
    $dvd->format_id = $request->input('format');

    $dvd->save();

    return redirect('dvds/create')->with('success', true);
  }

  public function create() 
  {
    return view('create', [
      'labels' => Label::all(),
      'sounds' => Sound::all(),
      'genres' => Genre::all(),
      'ratings' => Rating::all(),
      'formats' => Format::all()
    ]);
  }

  public function search()
  {
    return view('search', [
      'genres' => Genre::all(),
      'ratings' => Rating::all()
      ]);
  }

  public function results(Request $request)
  {
    $dvdTitle = $request->input('dvdTitle');
    $genre = $request->input('genre');
    $rating = $request->input('rating');

    $dvds = DB::table('dvds')
      ->select('dvds.id', 'title', 'rating_name', 'genre_name', 'label_name', 'sound_name', 'format_name')
      ->join('ratings', 'dvds.rating_id', '=', 'ratings.id')
      ->join('genres', 'dvds.genre_id', '=', 'genres.id')
      ->join('labels', 'dvds.label_id', '=', 'labels.id')
      ->join('sounds', 'dvds.sound_id', '=', 'sounds.id')
      ->join('formats', 'dvds.format_id', '=', 'formats.id');

    if ($dvdTitle !== "" && !empty($dvdTitle)) {
      $dvds = $dvds->where('title', 'LIKE', "%$dvdTitle%");
    }

    if ($genre !== 'All' && !empty($genre)) {
      $dvds = $dvds->where('genre_name', '=', $genre);
    }

    if ($rating !== 'All' && !empty($rating)) {
      $dvds = $dvds->where('rating_name', '=', $rating);
    }

    $dvds = $dvds->get();

    return view('results', [
      'dvdSearch' => $dvdTitle,
      'genreSearch' => $genre,
      'ratingSearch' => $rating,
      'dvds' => $dvds
    ]);
  }

  public function review($id)
  {
    $dvd = DB::table('dvds')
      ->select('dvds.id', 'title', 'release_date', 'award' ,
        'rating_name', 'genre_name', 'label_name', 'sound_name', 
        'format_name')
      ->join('ratings', 'dvds.rating_id', '=', 'ratings.id')
      ->join('genres', 'dvds.genre_id', '=', 'genres.id')
      ->join('labels', 'dvds.label_id', '=', 'labels.id')
      ->join('sounds', 'dvds.sound_id', '=', 'sounds.id')
      ->join('formats', 'dvds.format_id', '=', 'formats.id')
      ->where('dvds.id', '=', $id)
      ->get();

    return view('review', [
      'dvd' => $dvd[0],
      'reviews' => Review::reviewsById($id)
    ]);

  }
}
