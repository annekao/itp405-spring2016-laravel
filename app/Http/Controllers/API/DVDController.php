<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Dvd;
use App\Models\Genre;
use App\Models\Rating;

use Response;
use Validator;

class DVDController extends Controller
{
  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [
      'title' => 'required|unique:dvds,title'
    ]);

    if ($validation->passes()) {
      $dvd = new Dvd();
      $dvd->title = $request->input('title');
      $dvd->save();

      return [
        'dvd' => $dvd
      ];
    }

    return Response::json([
      'error' => [
        'title' => $validation->errors()->get('title')
      ]
    ], 422);
  }

  public function show($id)
  {

    $dvd = Dvd::find($id);

    if(!$dvd) {
      return Response::json([
        'error' => 'DVD not found'
      ], 404);
    }
    
    return [
      'dvd' => $dvd,
      'genres' => Genre::find($dvd->genre_id),
      'ratings' => Rating::find($dvd->rating_id)
    ];
  }

  public function index()
  {
    $dvds = Dvd::take(20)->get();
    $genres = [];
    $ratings = [];

    foreach($dvds as $dvd) {
      array_push($ratings, Rating::find($dvd->rating_id));
      array_push($genres, Genre::find($dvd->genre_id));
    }

    return [
      'dvds' => $dvds,
      'genres' => $genres,
      'ratings' => $ratings
    ];
  }
}
