<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BookSearchTest extends TestCase
{
  private $books = [
    [ 'title' => 'Introduction to HTML and CSS', 'pages' => 432 ],
    [ 'title' => 'Learning JavaScript Design Patterns', 'pages' => 32 ],
    [ 'title' => 'Object Oriented JavaScript', 'pages' => 42 ],
    [ 'title' => 'JavaScript Web Applications', 'pages' => 99 ],
    [ 'title' => 'PHP Object Oriented Solutions', 'pages' => 80 ],
    [ 'title' => 'PHP Design Patterns', 'pages' => 300 ],
    [ 'title' => 'Head First Java', 'pages' => 200 ]
  ]; 

  // test 1
  public function testBookSearchResults()
  {
    $search = new \App\Services\BookSearch($this->books);
    $results = $search->find('javascript');
    $prunedResults = [];

    // Write a unit test to verify that $results is 
    // a subset of $books containing the following titles:
    $expectedResults = [
      [ 'title' => 'Learning JavaScript Design Patterns', 'pages' => 32 ],
      [ 'title' => 'Object Oriented JavaScript', 'pages' => 42 ],
      [ 'title' => 'JavaScript Web Applications', 'pages' => 99 ],
    ];
    
    // Your unit test will have to search the array for 
    // titles containing 'javascript' and be case insensitive.
    foreach($results as $result) {
      $lowerTitle = strtolower($book->title);
      if (strpos($lowerTitle, 'javascript')) {
        $prunedResults[] = $book;
      }
    }

    $this->assertEquals($prunedResults, $expectedResults);
  }

  // test 2
  public function testBookSearchResultsExact()
  {
    $search = new \App\Services\BookSearch($this->books);
    $results = $search->find('javascript web applications', true);
    $prunedResults = [];

    $expectedResults = [
      [ 'title' => 'JavaScript Web Applications', 'pages' => 99 ],
    ];
    
    // It should take a 2nd parameter that denotes if it 
    // should search for an exact match, but again, case insensitive.
    foreach($results as $result) {
      $lowerTitle = strtolower($book->title);
      if (strpos($lowerTitle, 'javascript web applications')) {
        $prunedResults[] = $book;
      }
    }

    $this->assertEquals($prunedResults, $expectedResults);
  }

  // test 3
  public function testBookSearchResultsFailed()
  {
    $search = new \App\Services\BookSearch($books);
    // When we call find(), it should return false.
    $results = $search->find('The Definitive Guide to Symfony', true); // false
  
    $this->assertFalse($results);
  }
}
