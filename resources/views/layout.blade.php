<html>
    <head>
        <title>@yield('title')</title>
          <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
          <link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
          <link rel='stylesheet' type='text/css' href='/css/style.css'>
    </head>
    <body>
        <h1 class="books-title">@yield('title')</h1>
        <div class="books-container">
            @yield('content')
        </div>
    </body>
</html>
