<?php

namespace App\Services\API;
use Cache;

class NYTimes {
  protected $apiKey;

  public function __construct(array $config=[])
  {
    $this->apiKey = $config['apiKey'];
  }

  public function books($url, $date)
  {
    if (Cache::get($url)) {
      return json_decode(Cache::get($url))->results->books;
    } else {
      $apiKey = $this->apiKey;
      $lookupURL = "http://api.nytimes.com/svc/books/v3/lists/$date/combined-print-and-e-book-fiction.json?api-key=$apiKey";
      $jsonString = file_get_contents($lookupURL);
      Cache::put($url, $jsonString, 30);
    }

    $response = json_decode($jsonString);
    return $response->results->books;
  }
}
