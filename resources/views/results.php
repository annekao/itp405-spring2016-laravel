<html>
<head>
  <title>DVD Search</title>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
  <link rel='stylesheet' type='text/css' href='/css/style.css'>
</head>
<body>
  <meta charset="UTF-8">
  <title>DVD Search</title>
</head>
<body>
  <div id="outer-container2">
  <h2>
    You searched for '<?php echo $dvdSearch ?>'
    in the genre '<?php echo $genreSearch ?>'
    with the rating '<?php echo $ratingSearch ?>'.
  </h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Title</th>
        <th></th>
        <th>Rating</th>
        <th>Genre</th>
        <th>Label</th>
        <th>Sound</th>
        <th>Format</th>
      <tr>
    </thead>
    <tbody>
      <?php foreach ($dvds as $dvd) : ?>
        <tr>
          <th><?php echo $dvd->title; ?></th>
          <th><a href="/dvds/<?php echo $dvd->id ?>">Review</a></th>
          <th><?php echo $dvd->rating_name; ?></th>
          <th><?php echo $dvd->genre_name; ?></th>
          <th><?php echo $dvd->label_name; ?></th>
          <th><?php echo $dvd->sound_name; ?></th>
          <th><?php echo $dvd->format_name; ?></th>
        <tr>
    <?php endforeach; ?>
    </tbody>
  </table>
  </div>
</body>
</html>
