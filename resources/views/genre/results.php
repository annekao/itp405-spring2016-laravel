<html>
<head>
  <title>Genre Results</title>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
  <link rel='stylesheet' type='text/css' href='/css/style.css'>
</head>
<body>
  <meta charset="UTF-8">
</head>
<body>
  <div id="outer-container2">
  <h1>
    <?php echo $genre_name ?>
  </h1>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Title</th>
        <th>Rating</th>
        <th>Genre</th>
        <th>Label</th>
      <tr>
    </thead>
    <tbody>
      <?php foreach ($dvds as $dvd) : ?>
        <tr>
          <th><?php echo $dvd->title; ?></th>
          <th><?php if ($dvd->rating != null) echo $dvd->rating->rating_name ?></th>
          <th><?php if ($dvd->genre != null) echo $dvd->genre->genre_name; ?></th>
          <th><?php if ($dvd->label != null) echo $dvd->label->label_name; ?></th>
        <tr>
    <?php endforeach; ?>
    </tbody>
  </table>
  </div>
</body>
</html>
