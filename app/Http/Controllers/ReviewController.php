<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use App\Models\Review;

class ReviewController extends Controller
{
    public function store(Request $request, $id)
    {
      $request['dvd_id'] = $id;
      $validation = Validator::make($request->all(), [
        'rating' => 'required|digits_between:1,10',
        'title' => 'required|min:5',
        'description' => 'required|min:10',
        'dvd_id' => 'required|integer'
      ]);

      if ($validation->fails()) {
        return redirect('dvds/' . $id)
          ->withInput()
          ->withErrors($validation);
      }

      $review = new Review([
        'title' => $request->input('title'),
        'description' => $request->input('description'),
        'rating' => $request->input('rating'),
        'dvd_id' => $id
      ]);

      $review->save();
      
      return redirect('dvds/' . $id)->with('success', true);
    }
}
